# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=frameworkintegration
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework providing components to allow applications to integrate with a KDE Workspace"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11.
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 knotifications-dev
	kconfig-dev kconfigwidgets-dev kiconthemes-dev qt5-qtx11extras-dev
	knewstuff-dev kpackage-dev kxmlgui-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/frameworkintegration-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="9bb0c8a10b342976c21f8840e04f4b9ac60931c804fc0debbddf24b710e97804a2b8d357019840534c4fb4b4b816565a49682b0b09b4fb4d82d7d3467af1bb1f  frameworkintegration-5.54.0.tar.xz"
