# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=extra-cmake-modules
pkgver=5.65.0
pkgrel=0
pkgdesc="CMake modules needed for KDE development"
url="https://www.kde.org/"
arch="noarch"
options="!dbg"
license="BSD-3-Clause"
depends=""
makedepends="cmake qt5-qtbase-dev qt5-qttools-dev qt5-qtdeclarative-dev
	qt5-qtquickcontrols py3-sphinx"
subpackages="$pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/extra-cmake-modules-$pkgver.tar.xz
	posix.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE \
		ctest -E '(relative_or_absolute_|KDEFetchTranslations)'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="0e48374deaad34e3a9d9be1bba81890e40611ff2ccccb71d8f702d40fdb53c599404ace20ead4daf13951834cee6224093d20a83f0eb85ad2a6c973eab925ed4  extra-cmake-modules-5.65.0.tar.xz
a9e5d5e7ac8372099458ed18d2a6023fa0acf46955f51509880e7a467b4bd9e5df67c44c9ad032b1d70139efb73206390eaf7cd2baf63a82131e6e2b4acdbd71  posix.patch"
