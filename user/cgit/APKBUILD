# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer:
pkgname=cgit
pkgver=1.2.1
pkgrel=1
_gitver=2.18.0
pkgdesc="A fast Web interface for Git"
url="https://git.zx2c4.com/cgit/"
arch="all"
license="GPL-2.0-only"
makedepends="openssl-dev zlib-dev lua5.3-libs lua5.3-dev asciidoctor gettext-tiny-dev"
subpackages="$pkgname-doc"
source="http://git.zx2c4.com/$pkgname/snapshot/$pkgname-$pkgver.tar.xz
	https://www.kernel.org/pub/software/scm/git/git-$_gitver.tar.xz
	"

# Git makeopts come first
_makeopts="NO_CURL=1
	NO_REGEX=NeedsStartEnd
	NO_SVN_TESTS=YesPlease
	NO_SYS_POLL_H=1
	NO_TCLTK=YesPlease
	ASCIIDOC=asciidoctor
	LUA_PKGCONFIG=lua
	prefix=/usr"

prepare() {
	cd "$builddir"

	# check that upstream git ver corresponds with ours
	local _ver="$(awk -F'[ \t]*=[ \t]*' '/^GIT_VER/ { print $2 }' Makefile)"
	if [ "$_ver" != "$_gitver" ]; then
		error "Please set _gitver in APKBUILD to $_ver"
		return 1
	fi

	rm -rf git
	mv ../git-$_gitver git

	default_prepare
}

build() {
	cd "$builddir"
	sed -i -e 's:a2x -f:asciidoctor -b:' Makefile
	make $_makeopts all doc-man
}

check() {
	cd "$builddir"
	make $_makeopts test
}

package() {
	cd "$builddir"
	make $_makeopts DESTDIR="$pkgdir" \
		CGIT_SCRIPT_PATH=/usr/share/webapps/cgit \
		install install-man
	ln -s cgit.cgi "$pkgdir"/usr/share/webapps/cgit/cgit
}

sha512sums="c7380df9afbc3735ef9e4f196f4f5bbd26cf52e473fa6f435e7d0c00dc295cc8de6bee6bfb1857144025c8591d41a6a74efb1af551e8610848d90ac40fcfab36  cgit-1.2.1.tar.xz
db19363c9c2042248322d49874a27c0614acfb912183725e5d4f0331d6b44cef66a9a7da6a49bd4a17e5d86d30c5fed6bef7527f386494184595a433c4060e46  git-2.18.0.tar.xz"
