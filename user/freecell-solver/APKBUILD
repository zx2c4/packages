# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=freecell-solver
pkgver=4.20.0
pkgrel=0
pkgdesc="Library for solving card games"
url="https://fc-solve.shlomifish.org/"
arch="all"
license="MIT"
depends="python3"
makedepends="gperf perl perl-task-freecellsolver-testing python3 py3-random2
	py3-six"
checkdepends="gmp-dev libtap-dev perl-dev py3-cffi py3-pycotap the_silver_searcher
	valgrind"
subpackages="$pkgname-dev $pkgname-doc"
source="https://fc-solve.shlomifish.org/downloads/fc-solve/freecell-solver-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	# The build system generates header files; jobs >1 may cause failures
	# because .h don't exist yet.
	make -j1
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="6ee43c79e2562ee2dcff96301ebb7c98e619393e8bb9396871d8cac9e231b3666480202938e30eabbc3a3d2c8beca8f01e5f6bf9365a2a575197bd9fd3a0c570  freecell-solver-4.20.0.tar.xz"
