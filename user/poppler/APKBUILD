# Maintainer: 
pkgname=poppler
pkgver=0.83.0
pkgrel=0
pkgdesc="PDF rendering library based on xpdf 3.0"
url="https://poppler.freedesktop.org/"
arch="all"
options="!check"  # Tests only cover Qt5 component.
license="GPL-2.0+"
depends=""
depends_dev="cairo-dev glib-dev"
makedepends="$depends_dev libjpeg-turbo-dev cairo-dev libxml2-dev openjpeg-dev
	fontconfig-dev gobject-introspection-dev lcms2-dev libpng-dev tiff-dev
	zlib-dev cmake"
subpackages="$pkgname-dev $pkgname-doc $pkgname-utils $pkgname-glib"
source="https://poppler.freedesktop.org/poppler-$pkgver.tar.xz"
builddir="$srcdir"/$pkgname-$pkgver/build

# secfixes:
#   0.77.0-r0:
#     - CVE-2019-9200
#     - CVE-2019-9631
#     - CVE-2019-9903
#     - CVE-2019-10872
#     - CVE-2019-10873
#     - CVE-2019-11026
#     - CVE-2019-12293
#   0.80.0-r0:
#     - CVE-2019-9959
#     - CVE-2019-14494

prepare() {
	default_prepare
	mkdir "$builddir"
}

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DENABLE_UNSTABLE_API_ABI_HEADERS=ON \
		..
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

utils() {
	pkgdesc="Poppler's xpdf-workalike command line utilities"
	install -d "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

glib() {
	pkgdesc="Glib wrapper for poppler"
	replaces="poppler-gtk"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libpoppler-glib.so.* \
		"$pkgdir"/usr/lib/girepository* \
		"$subpkgdir"/usr/lib/
}

sha512sums="bfde1e3cc4d9c626949aab770d84fccbe47bdde2331d3b9f5b98c24319eb573a5db62acbfa5a524b21ed0f195f8b163fee64677c2bd616d31d0bb219d1e66713  poppler-0.83.0.tar.xz"
