# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=marble
pkgver=19.08.2
pkgrel=0
pkgdesc="Free, open-source map and virtual globe"
url="https://marble.kde.org/"
arch="all"
options="!check"  # Test suite requires package to be already installed.
license="LGPL-2.1-only AND GPL-2.0-only"
depends="shared-mime-info"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev zlib-dev
	qt5-qtserialport-dev krunner-dev kcoreaddons-dev kwallet-dev knewstuff-dev
	kio-dev kparts-dev kcrash-dev ki18n-dev phonon-dev plasma-framework-dev
	qt5-qtpositioning-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
source="https://download.kde.org/stable/applications/$pkgver/src/marble-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="d18cfa59be6b360271afc711f61925a09dc4a304eaf41abbaeb6c7c6dc5aedff35eab658c47abba6fa5919940b6a7236292d38cc2e6eafe792c4188f090c1c61  marble-19.08.2.tar.xz"
