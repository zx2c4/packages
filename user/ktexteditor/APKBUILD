# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ktexteditor
pkgver=5.54.0
pkgrel=0
pkgdesc="Reusable, programmable text editor widget"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running X11 session.
license="LGPL-2.1-only AND LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtxmlpatterns-dev
	karchive-dev kconfig-dev kguiaddons-dev ki18n-dev kio-dev kparts-dev
	syntax-highlighting-dev libgit2-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ktexteditor-$pkgver.tar.xz"

# secfixes:
#   5.52.0-r0:
#     - CVE-2018-10361

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="8402fb036887ef853e455b4c80b3f4ff7fa6a65fc37e94d4fb2e94c09a59f2e49d7403dbf94aa3f2b2eb7ac6f43c5aede9aaa45ab5e9597a62b8a5d9b7ad0500  ktexteditor-5.54.0.tar.xz"
