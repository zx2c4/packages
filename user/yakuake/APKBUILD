# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=yakuake
pkgver=19.08.2
pkgrel=0
pkgdesc="Drop-down KDE terminal emulator"
url="https://www.kde.org/applications/system/yakuake/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kconfig-dev
	kcoreaddons-dev kcrash-dev kdbusaddons-dev kglobalaccel-dev ki18n-dev
	kiconthemes-dev kio-dev knewstuff-dev knotifications-dev kparts-dev
	knotifyconfig-dev kwidgetsaddons-dev kwindowsystem-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/yakuake-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="fba00f8658ed799af1d7fe2e486fb450e456f6161e69bbf61bea344f4b58e9cc5933cf9306a89863440610f5f394431ee14fbd5605b2a929a40073dbdf902351  yakuake-19.08.2.tar.xz"
