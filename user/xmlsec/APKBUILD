# Maintainer: Max Rees <maxcrees@me.com>
pkgname=xmlsec
pkgver=1.2.28
_pkgname="$pkgname${pkgver%%.*}"
pkgrel=1
pkgdesc="C-based XML signature and encryption syntax and processing library"
url="https://www.aleksey.com/xmlsec/"
arch="all"
license="MIT"
depends=""
checkdepends="nss-tools"
makedepends="libxml2-dev libxslt-dev openssl-dev
	gnutls-dev libgcrypt-dev nss-dev"
subpackages="$pkgname-dev $pkgname-doc
	$pkgname-gcrypt $pkgname-gnutls $pkgname-nss"
source="http://www.aleksey.com/xmlsec/download/$_pkgname-$pkgver.tar.gz
	fix-tests.patch"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-static \
		--enable-pkgconfig \
		--with-openssl \
		--with-gnutls \
		--with-gcrypt \
		--with-default-crypto='openssl'
	make
}

check() {
	make -k check
}

package() {
	make DESTDIR="$pkgdir" install

	install -m755 -d "$pkgdir/usr/share/licenses/$pkgname"
	install -m644 'COPYING' "$pkgdir/usr/share/licenses/$pkgname/"
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/xmlsec1Conf.sh "$subpkgdir"/usr/lib
}

gcrypt() {
	pkgdesc="xmlsec gcrypt plugin"
	install_if="$pkgname=$pkgver-r$pkgrel gcrypt"
	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libxmlsec1-gcrypt.so* "$subpkgdir"/usr/lib/
}

gnutls() {
	pkgdesc="xmlsec gnutls plugin"
	install_if="$pkgname=$pkgver-r$pkgrel gnutls"
	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libxmlsec1-gnutls.so* "$subpkgdir"/usr/lib/
}

nss() {
	pkgdesc="xmlsec NSS plugin"
	install_if="$pkgname=$pkgver-r$pkgrel nss"
	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libxmlsec1-nss.so* "$subpkgdir"/usr/lib/
}

sha512sums="17fa59e4ffee5e024caa4895e8ed21d1435f14e3a37d0ed781b1dd216333ae3b6099c460efd45d4a8097d0202522150b7b0ad543b47c1596d8473b6922270480  xmlsec1-1.2.28.tar.gz
2f146b31460ae1843a696c77cef03b36a0bf212028189055834e2e50d7a3142f6a069ce18a5c6b2251b5846fad7e96a6d9e26a6445fd182ac4c44c70afc4f8a3  fix-tests.patch"
