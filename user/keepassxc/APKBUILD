# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=keepassxc
pkgver=2.4.3
pkgrel=0
pkgdesc="A community revival of the KeePassX password manager"
url="https://keepassxc.org"
arch="all"
license="(GPL-2.0-only OR GPL-3.0-only) AND MIT AND BSD-4-Clause AND ISC AND (LGPL-2.1-only OR GPL-3.0-only) AND (LGPL-2.1-only OR LGPL-3.0-only) AND CC0-1.0 AND Public-Domain AND LGPL-2.1+ AND LGPL-3.0+"
depends="hicolor-icon-theme"
makedepends="argon2-dev cmake libgcrypt-dev libqrencode-dev libsodium-dev
	libxi-dev libxtst-dev qt5-qtbase-dev qt5-qtsvg-dev qt5-qttools-dev
	qt5-qtx11extras-dev xz zlib-dev"
subpackages="$pkgname-doc"
source="https://github.com/keepassxreboot/$pkgname/releases/download/$pkgver/$pkgname-$pkgver-src.tar.xz"

prepare() {
	default_prepare
	mkdir build
}

build() {
	cd "$builddir/build"
	cmake -DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo ..
	make
}

check() {
	cd "$builddir/build"
	make test
}

package() {
	cd "$builddir/build"
	make DESTDIR="$pkgdir" install
}

sha512sums="893f1d18ab8051143d29c568ba87adcc42a13d28d0c3a7af04396cd91d6724f8a98d76d2e20ca15138c4642ec060d48b9e957857251f3a0df6066af08cd0765d  keepassxc-2.4.3-src.tar.xz"
