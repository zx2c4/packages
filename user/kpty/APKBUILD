# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpty
pkgver=5.65.0
pkgrel=0
pkgdesc="Framework for implementing terminal emulation"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1-only GPL-2.0+"
depends="libutempter"
depends_dev="qt5-qtbase-dev kcoreaddons-dev ki18n-dev libutempter-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpty-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		-DUTEMPTER_EXECUTABLE=/usr/lib/utempter/utempter \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="04b3ae8a2ff00219ad1e712b3f7ffa2fad95431bc67f6ed4a609dc93fc85fea357669f00dd0d8510392c3c9f938cb0ed7ab84c398827299ef530b8d29cdec592  kpty-5.65.0.tar.xz"
