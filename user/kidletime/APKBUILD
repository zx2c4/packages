# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kidletime
pkgver=5.65.0
pkgrel=0
pkgdesc="Framework for determining a user's idle time"
url="https://api.kde.org/frameworks/kidletime/html/index.html"
arch="all"
license="LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtx11extras-dev"
makedepends="$depends_dev cmake extra-cmake-modules libx11-dev libxext-dev
	qt5-qttools-dev doxygen libxscrnsaver-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kidletime-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b3a31a3813571ab415020e7653b04768e99fe2347dd9b2eabaadba2054015e5b157f95a89857d26911133e50a643d2ba656e22f72ca1fce918bcbd9faec10aa1  kidletime-5.65.0.tar.xz"
