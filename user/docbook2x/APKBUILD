# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=docbook2x
pkgver=0.8.8
pkgrel=0
pkgdesc="Tool to convert DocBook XML to Unix man pages"
url="http://docbook2x.sourceforge.net/"
arch="all"
license="MIT"
depends="docbook-xml docbook-xsl libxml2-utils libxslt perl-xml-sax"
makedepends="$depends_dev"
subpackages="$pkgname-doc"
source="https://downloads.sourceforge.net/docbook2x/docbook2X-$pkgver.tar.gz
	autoconf.patch
	doc-typo.patch
	manpage-comments.patch
	refentry-whitespace.patch
	sourceforge-url.patch
	"
builddir="$srcdir/docbook2X-$pkgver"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--program-transform-name 's/docbook2/docbook2x-/'
	make
}

check() {
	cd "$builddir"/test/refentry
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="cc336017ad734fc62d96d289105e8ea154c418a03a37f3e21b0b3520063f8b466b4aae5a5aec2e0b83f6324c6c79b5557247a93338d0a9882a94a44112f6b65c  docbook2X-0.8.8.tar.gz
eec73ea9e5b626dbc7d0107226ea15dd1b84194f83d97bd93b52d61acc6e03e7d683c6b52d3bd191a995db034dd09044005a18b565b9f55fe5ec2824eec0b639  autoconf.patch
a563b46e7eaa8052dc2daea6ad8b0f3b12780ef063fafd37a6345ae663f6229ccb0b52be5e7b1fd6584d31e56de89af391efb856bbabfed164353578b39fb458  doc-typo.patch
f204384bb206324d813b36e4e87e6a96f8b5808bfb3d7396505c84efc6784d4950a87533a3be1c67fce5f7b1d2f76eee8cf57f3ae48e3df43cf0cdc6531fbe14  manpage-comments.patch
10ce7f7da9bb2e05701a38d9e6900e87e62043604736eef5dbb52d8b143c8693463cffacf24453ec3d1e057b0b243d1c53952247f412e6f036c7067ad5840c1f  refentry-whitespace.patch
c9dfbc19fc6bd11980c6da264e2683fd3c320f8c5d0d35f6c6d322cf3e7341d2d3fc88498e4a0213d4191f83c238abb0d824630be1360b3ec58e9887e8309aaa  sourceforge-url.patch"
