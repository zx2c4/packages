# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=r
pkgver=3.6.1
pkgrel=0
pkgdesc="Environment for statistical computing and graphics"
url="https://www.r-project.org/"
arch="all"
options="!check"  # 20.482886 != 20.482887
license="GPL-2.0-only"
depends="cmd:which"
makedepends="byacc bzip2-dev cairo-dev curl-dev gfortran icu-dev libice-dev
	libtirpc-dev libx11-dev libxt-dev pango-dev pcre-dev pcre2-dev xz-dev
	zlib-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://rweb.crmda.ku.edu/cran/src/base/R-3/R-$pkgver.tar.gz"
builddir="$srcdir/R-$pkgver"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--docdir=/usr/share/doc/R-$pkgver \
		--localstatedir=/var \
		--with-readline=no \
		--enable-R-shlib
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="fae7d114437c4b5d10fcb2a4265acd707ad2a4810eeb7c11e25caca1d85093a1495c2e13457b4894c1508830ec1b2be95379b7d54007cf3e574c2a9eea28ef80  R-3.6.1.tar.gz"
