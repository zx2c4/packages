# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=iperf3
_pkgname=iperf
pkgver=3.7
pkgrel=0
pkgdesc="A tool to measure IP bandwidth using UDP or TCP"
url="https://software.es.net/iperf/"
arch="all"
license="BSD-3-Clause"
depends=""
makedepends=""
install=""
subpackages="$pkgname-doc $pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://downloads.es.net/pub/$_pkgname/$_pkgname-$pkgver.tar.gz
	$_pkgname.initd
	$_pkgname.confd

	build-fixes.patch"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	cd "$builddir"
	CFLAGS="$CFLAGS -D_GNU_SOURCE"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-profiling
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	install -Dm755 "$srcdir"/$_pkgname.initd \
		"$pkgdir"/etc/init.d/$_pkgname
	install -Dm644 "$srcdir"/$_pkgname.confd \
		"$pkgdir"/etc/conf.d/$_pkgname
}

sha512sums="9b8d1ac6bdebb00e38e196db9a71ae6ff3b8b763cd4b1336d078c31f6d0a5ab53bbe70ba949c601af5efe674dbd032ed5d58a761d5159dae4e50122b68a8f086  iperf3-3.7.tar.gz
339fb04b41cce11e8ea8694d95c13af9c88e0d1143356d913ec810c9b11efa27212d585b5dcc49cc2eb860a6d8bc557092c8659d7d5cfe532c5afdb36f8eedf8  iperf.initd
fb5e155fff568a72488cc1511d30358835a48bcce309f8f46d040160932b4a4ccb85040c27c60dee477900a25bd34c50eac21d1d0bea984b40faa401be2ba7e8  iperf.confd
126043503eebf84bccf26758f18715eaf81131e2fc9ef1fe829e55acb2f14e97db18e9b890b8bf13c230ac80c7537615272af4e072808e25bde8ddd6f326f198  build-fixes.patch"
