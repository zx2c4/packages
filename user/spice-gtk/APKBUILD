# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=spice-gtk
pkgver=0.37
pkgrel=0
pkgdesc="A GTK+ widget for SPICE clients"
url="https://www.spice-space.org/"
arch="all"
# suid: ACL helper for USB redirection
options="suid"
license="LGPL-2.1+ AND LGPL-2.0+ AND BSD-3-Clause AND MIT AND GPL-3.0+ AND LGPL-2.0-only AND GPL-2.0+"
depends="gst-plugins-good"
depends_dev="gobject-introspection-dev gtk+3.0-dev"
makedepends="$depends_dev acl-dev bash cyrus-sasl-dev eudev-dev
	gst-plugins-base-dev gstreamer-dev gstreamer-tools json-glib-dev
	libjpeg-turbo-dev libusb-dev libxrandr-dev lz4-dev openssl-dev
	opus-dev polkit-dev polkit-dev spice-protocol usbredir-dev
	usbutils zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang spicy
	spice-glib:glib"
source="https://www.spice-space.org/download/gtk/$pkgname-$pkgver.tar.bz2"

build() {
	# Note: pulseaudio support is disabled because it's deprecated.
	# Audio is still supported through gstreamer.
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--with-gtk=3.0 \
		--with-audio=gstreamer \
		--disable-celt051 \
		--disable-werror \
		--enable-lz4 \
		--enable-opus \
		--enable-smartcard=no \
		--enable-usbredir=yes \
		--enable-polkit=yes \
		--enable-pulse=no
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

spicy() {
	pkgdesc="SPICE client (remote virtual machine access)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

glib() {
	pkgdesc="$pkgdesc (GLib libraries)"
	mkdir -p "$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/lib/girepository-1.0/
	mv "$pkgdir"/usr/lib/*-glib-*.so* \
		"$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/girepository-1.0/SpiceClientGLib-*.typelib \
		"$subpkgdir"/usr/lib/girepository-1.0/
}

sha512sums="a0a20bc6f25337d86e57fe1fc9586c4cc84457fc8c38cdcc5a728990a69018da0fca3ab5aa63349786b5a7508c82b716c94803eefb3495cffb7df4526db2d029  spice-gtk-0.37.tar.bz2"
