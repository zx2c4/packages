# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=nano
pkgver=4.5
pkgrel=0
pkgdesc="Enhanced clone of the Pico text editor"
url="https://www.nano-editor.org"
arch="all"
license="GPL-3.0+"
makedepends="file-dev ncurses-dev"
subpackages="$pkgname-doc $pkgname-lang"
replaces="nano-syntax"  # ??? why was this ever split
source="https://www.nano-editor.org/dist/v${pkgver%.*}/$pkgname-$pkgver.tar.xz"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-wordbounds
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"

	make DESTDIR="$pkgdir" install
	install -Dm644 doc/sample.nanorc \
		"$pkgdir"/etc/nanorc

	# Proper syntax highlighting for APKBUILDs
	sed -i "/syntax/s/\"$/\" \"APKBUILD&/" \
		"$pkgdir"/usr/share/nano/sh.nanorc

	rm -rf "$pkgdir"/usr/lib/charset.alias
}

sha512sums="1caee1f559cb11efa7535f4682e7ff6e22802d01ca21733a67031e4ba7c5d3ca13c60f22f71460bb99eadd8d001aeb3783a5e34d1433583e8822fc13d063e4d9  nano-4.5.tar.xz"
