# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tigervnc
pkgver=1.9.0
pkgrel=4
pkgdesc="High-performance, platform-neutral VNC remote desktop application"
url="https://tigervnc.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0"
depends=""
makedepends="cmake fltk-dev fontconfig-dev gnutls-dev intltool
	libjpeg-turbo-dev libx11-dev libxcursor-dev libxfixes-dev libxft-dev
	libxrender-dev libxtst-dev linux-pam-dev zlib-dev"
subpackages="$pkgname-lang $pkgname-doc"
source="tigervnc-$pkgver.tar.gz::https://github.com/TigerVNC/tigervnc/archive/v$pkgver.tar.gz
	use-intltool.patch
	endian.patch
	0001-CSecurityTLS-Use-size_t-as-argument-for-new.patch
	null-termination.patch
	initialise-var.patch
	ensure-throw.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -faligned-new -Wno-c++11-compat -Wno-maybe-uninitialized" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-Wno-dev \
		${CMAKE_CROSSOPTS}
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="333910f567e6b5e4a5a22d898b2d4c3f4b834cb4cc8fc13ff55d31401894c0d5122a127692ec5eb51e412c945ff3ea5b8146f9ab22cbe1e47541e09239ec8c9d  tigervnc-1.9.0.tar.gz
5c1cee98b7ba41c7cf121480fdfe16d5ef17c9562ff2ba3ea4e74235161fc63e2e3ed63e788c0aa999610b660b394c1269d6fdcc9716c5563651fd67d723f619  use-intltool.patch
189a51a542b368e4db22174d09f5b656848e94577bbf93b2388f54529f1c7c2d32e5b5283551b3fb067ba21f6464f60989e22d4cd11ed3d87d5c931301555b49  endian.patch
f95328f6b669e6608b9971de3db25d5eb26a733fbe32f13291c309ed57eacba6c86461a516c3b8cdc12ff7482ee0249a45189864d473d52df81df0a3541d95b9  0001-CSecurityTLS-Use-size_t-as-argument-for-new.patch
82566734e5288e899048b918ce47f5abc3b94196e88d54fa0b0ef68c20d1fcd9d5854f36a0043d28f81dc6213c9ee8cf55fb187aeeb8add5fc31765f6a5b23bb  null-termination.patch
a7fb612d7e3625b66db66ffc7e3f28f2e47f79b858ff1fd9e403576ef2b8ff7bc8cf83a6f67439de6242e4a11a0144119d040f8f9e917e17a8af512b47067360  initialise-var.patch
4656a617dcb2bd28b653932fa034b7d46c49a08be4ba90501a9d1ddfad2130209664abf7bf5227f0afc926a568eb83e8e5f25e73887bf2561b2077fb1dab84c9  ensure-throw.patch"
