#!/sbin/openrc-run

description="Start/stop the s6 services for netqmail"

depend()
{
  after net
  after localmount
}

makesmtpd()
{
  set -e
  ip="$1"
  usetls="$2"
  if "$usetls" ; then
    s="s"
    port=25
  else
    s=
    port=465
  fi
  if s6-tcpserver6-socketbinder -dBb0 -- "$ip" "$port" true 2>/dev/null ; then
    ipv=6
    relayfor="$smtprelay6"
  elif s6-tcpserver4-socketbinder -dBb0 -- "$ip" "$port" true 2>/dev/null ; then
    ipv=4
    relayfor="$smtprelay4"
  else
    return 1
  fi

  service="smtp${s}d-$ip"
  sdir=/run/services/"$service"
  logdir=/var/log/"smtp${s}d-$ip"

  mkdir -p -m 0755 "$logdir"
  chown qmaill:qmaill "$logdir"
  chmod 2700 "$logdir"

  rm -rf "$sdir"
  cp -a /etc/qmail/services/smtp"${s}"d"${ipv}"-skeleton "$sdir"
  echo "$ip" > "$sdir"/env/IP
  if "$usetls" ; then
    echo "$SMTPSD_KEYFILE" > "$sdir"/env/KEYFILE
    echo "$SMTPSD_CERTFILE" > "$sdir"/env/CERTFILE
  fi

  for j in $relayfor ; do
    mkdir -p -m 0755 "$sdir/data/rules/ip${ipv}/$j/env"
    touch "$sdir/data/rules/ip${ipv}/$j/allow"
    echo > "$sdir/data/rules/ip${ipv}/$j/env/RELAYCLIENT"
  done

  return 0
}

stop()
{
  set -e
  . /etc/conf.d/netqmail
  ebegin "Stopping SMTP and SMTPS listeners"
  dirs=""
  logs=""
  for i in ${SMTPD_IPS} ; do
    rm -f "/run/service/smtpd-$i"
    s6-svc -d "/run/services/smtpd-$i"
    dirs="$dirs /run/services/smtpd-$i"
    logs="$logs /run/services/smtpd-$i/log"
  done
  if test -r "$SMTPSD_KEYFILE" && test -r "$SMTPSD_CERTFILE" ; then
    for i in ${SMTPSD_IPS} ; do
      rm -f "/run/service/smtpsd-$i"
      s6-svc -d "/run/services/smtpsd-$i"
      dirs="$dirs /run/services/smtpsd-$i"
      logs="$logs /run/services/smtpsd-$i/log"
    done
  fi
  s6-svwait -D $dirs
  eend $?

  ebegin "Stopping the qmail-send service (this may take some time)"
  rm -f /run/service/qmail
  logs="$logs /var/qmail/services/qmail/log"
  s6-svc -dwD /var/qmail/services/qmail
  eend $?

  ebegin "Stopping loggers and cleaning up qmail services"
  for i in $logs ; do
    s6-svc -d "$i"
  done
  s6-svwait -D $logs
  s6-svscanctl -aN /run/service
  rm -rf $dirs
  eend $?
}

start()
{
  set -e
  . /etc/conf.d/netqmail

  ebegin "Parsing the relay subnets"
  smtprelay4=
  smtprelay6=
  for i in ${RELAY_SUBNETS} ; do
    j=`echo "$i" | tr / _`
    if echo "$i" | grep -qF : ; then
      smtprelay6="$smtprelay6 $j"
    elif echo "$i" | grep -qF . ; then
      smtprelay4="$smtprelay4 $j"
    else
      false
      break
    fi
  done
  eend $?

  ebegin "Starting the qmail-send service"
  ln -nsf ../../var/qmail/services/qmail /run/service/qmail
  s6-svscanctl -a /run/service
  s6-svwait -U /var/qmail/services/qmail
  eend $?

  for i in ${SMTPD_IPS} ; do
    ebegin "Creating a service directory for a SMTP listener on $i"
    makesmtpd "$i" false
    eend $?
  done
  if test -r "$SMTPSD_KEYFILE" && test -r "$SMTPSD_CERTFILE" ; then
    for i in ${SMTPSD_IPS} ; do
      ebegin "Creating a service directory for a SMTPS listener on $i"
      makesmtpd "$i" true
      eend $?
    done
  fi

  dirs=""
  ebegin "Starting SMTP/S listeners"
  for i in ${SMTPD_IPS} ; do
    ln -nsf "../services/smtpd-$i" "/run/service/smtpd-$i"
    dirs="$dirs /run/service/smtpd-$i"
  done
  if test -r "$SMTPSD_KEYFILE" && test -r "$SMTPSD_CERTFILE" ; then
    for i in ${SMTPSD_IPS} ; do
      ln -nsf "../services/smtpsd-$i" "/run/service/smtpsd-$i"
      dirs="$dirs /run/service/smtpsd-$i"
    done
  fi
  s6-svscanctl -aN /run/service
  s6-svwait -U $dirs
  eend $?
}
