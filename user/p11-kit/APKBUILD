# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: 
pkgname=p11-kit
pkgver=0.23.18.1
pkgrel=0
pkgdesc="Library for loading and sharing PKCS#11 modules"
url="https://p11-glue.github.io/p11-glue/p11-kit.html"
arch="all"
license="BSD-3-Clause"
depends=""
makedepends="libffi-dev libtasn1-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-trust"
source="https://github.com/p11-glue/p11-kit/releases/download/$pkgver/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--sysconfdir=/etc \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--with-trust-paths=/etc/ssl/certs/ca-certificates.crt
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

trust() {
	pkgdesc="System trust module from $pkgname"
	mkdir -p "$subpkgdir"/usr/share/p11-kit/modules \
		"$subpkgdir"/usr/lib/pkcs11

	mv "$pkgdir"/usr/share/p11-kit/modules/p11-kit-trust.module \
		"$subpkgdir"/usr/share/p11-kit/modules/
	mv "$pkgdir"/usr/lib/pkcs11/p11-kit-trust.so \
		"$subpkgdir"/usr/lib/pkcs11/
}

sha512sums="941996aea75025dad5dfc6f9b4609bc92bc0888d5b2b58de0d498d6cf75d059421405d9c3bad51122108b6ad1e41e434609ead23404efe635cdb806836ccadbd  p11-kit-0.23.18.1.tar.gz"
