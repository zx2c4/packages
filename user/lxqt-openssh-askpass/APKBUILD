# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lxqt-openssh-askpass
pkgver=0.14.1
pkgrel=0
pkgdesc="Graphical LXQt utility for inputting passwords for SSH agents"
url="https://lxqt.org"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qttools-dev lxqt-build-tools>=0.6.0
	liblxqt-dev>=${pkgver%.*}.0 kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxde/lxqt-openssh-askpass/releases/download/$pkgver/lxqt-openssh-askpass-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="ab35cd719d4a31a495bed68a30cad5bf85701d223ee7306f0dad40717d0c2821af53e07a929b5855fb0cd680684fb790a638b59a1ea3ef288e0f656ab490b5bf  lxqt-openssh-askpass-0.14.1.tar.xz"
