# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdesignerplugin
pkgver=5.54.0
pkgrel=0
pkgdesc="Qt Designer plugin for KDE widgets"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires accelerated X11 display.
license="LGPL-2.1-only"
depends=""
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev kio-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kdoctools-dev kiconthemes-dev kitemviews-dev kplotting-dev
	ktextwidgets-dev kwidgetsaddons-dev kxmlgui-dev sonnet-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdesignerplugin-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="ac566101b0b991376d31612033a86a8db6bce30734fed3a6022f6d6cfde812b6df6a65f73578ebd6fa31b3954a1af7b134e38bf795b512cf5aa4af684b87c1a7  kdesignerplugin-5.54.0.tar.xz"
