# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libktorrent
pkgver=2.1.1
pkgrel=0
pkgdesc="Torrent handling library for KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev karchive-dev kcrash-dev kio-dev solid-dev gmp-dev
	boost-dev qca-dev libgcrypt-dev"
makedepends="cmake extra-cmake-modules $depends_dev ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/ktorrent/5.1.2/libktorrent-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5c7f0218193cae148b03ae86a886b89d08a46200755728468ef271a6201add88eca617898d486d1823699def282d0251e26e3b28d2642f45cab966c286127d37  libktorrent-2.1.1.tar.xz"
