# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=freealut
pkgver=1.1.0_git20140626
pkgrel=0
pkgdesc="Free software implementation of OpenAL ALUT"
url="https://github.com/vancegroup/freealut"
arch="all"
license="LGPL-2.0-only"
depends=""
makedepends="openal-soft-dev"
subpackages="$pkgname-dev"
source="https://distfiles.adelielinux.org/source/freealut-$pkgver.tar.xz"

prepare() {
	cd "$builddir"
	default_prepare
	mkdir build
}

build() {
	cd "$builddir"/build
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="218f52ddc5f26aa6744d4209bb2ec6963e423dccec642ba96f9b6d0c1ee0972b9b86cd2fc4c81e21ba2fce7b6ff5e5150d23083b76602d0704a7e65b52f2cc45  freealut-1.1.0_git20140626.tar.xz"
