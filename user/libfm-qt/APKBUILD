# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=libfm-qt
pkgver=0.14.1
pkgrel=0
pkgdesc="Qt library for file management and bindings for libfm"
url="https://lxqt.org"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="libfm-dev menu-cache-dev libexif-dev"
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.6.0 qt5-qttools-dev
	qt5-qtx11extras-dev $depends_dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/libfm-qt/releases/download/$pkgver/libfm-qt-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -O0" \
		-DCMAKE_C_FLAGS="$CFLAGS -O0" \
		-DPULL_TRANSLATIONS=False \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir/build"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir/build"
	make DESTDIR="$pkgdir" install
}

sha512sums="a265f6831a077ccb78a994828e6c69a22f2f6432b1f6ed6f404af41f013112870f7aee98067f2c466bec6cdfea040c10c7ce7e0f0ed977e5d266ec38e543d2a7  libfm-qt-0.14.1.tar.xz"
