# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: 
pkgname=nettle
pkgver=3.5.1
pkgrel=0
pkgdesc="Low-level cryptographic library"
url="http://www.lysator.liu.se/~nisse/nettle/"
arch="all"
license="LGPL-3.0+ OR GPL-2.0+"
depends=""
depends_dev="gmp-dev"
makedepends="$depends_dev m4 openssl-dev texinfo"
subpackages="$pkgname-dev $pkgname-utils"
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.gz
	nettle-2.4-makefile.patch"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--libdir=/usr/lib \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-shared
	make
	# strip comments in fields from .pc as it confuses pkgconf
	sed -i -e 's/ \#.*//' *.pc
}

check() {
	make -C examples  # required for rsa-encrypt
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

utils() {
	pkgdesc="Utilities built with Nettle"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="f738121b9091cbe79435fb5d46b45cf6f10912320c233829356908127bab1cac6946ca56e022a832380c44f2c10f21d2feef64cb0f4f41e3da4a681dc0131784  nettle-3.5.1.tar.gz
c7d9741a7a37d225f3f0db16d355e13b04cc0f1ac56882a6ff31ef15c1a1a0aee7a70cf1ec8bbf2c46b9b0dcec153da7a7aa6b8909a72d76dd4d669cbbaceaa4  nettle-2.4-makefile.patch"
