# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=okular
pkgver=19.08.2
pkgrel=0
pkgdesc="Universal document reader developed by KDE"
url="https://okular.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+"
depends="kirigami2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	qt5-qtdeclarative-dev karchive-dev kbookmarks-dev kcompletion-dev
	kconfigwidgets-dev kcoreaddons-dev kdoctools-dev kiconthemes-dev
	kio-dev kjs-dev kparts-dev kwallet-dev kwindowsystem-dev khtml-dev
	threadweaver-dev kactivities-dev poppler-qt5-dev tiff-dev qca-dev
	libjpeg-turbo-dev kpty-dev kirigami2-dev djvulibre-dev libkexiv2-dev
	libspectre-dev ebook-tools-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/okular-$pkgver.tar.xz
	es-doc-fix.patch
	pt-doc-fix.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -D_GNU_SOURCE" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	# All other tests require X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -R '^shelltest'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="fbf6285e65f45b2d49596169216ded2da26a73abec1d2279bc510db5df0bb6dbf9794e864de06837c5b7bf1db19de1774c38c18eb78c4b0c2d3ac49cb35bb9f7  okular-19.08.2.tar.xz
d82dd9de666a28ef605d8a81e74851f265be4ccaeaa39a1cdee9eb1db830fcd0d581d01d6e89a1d36b8ea5c9c8113f1016090dc0e9f17070d388166d9e967458  es-doc-fix.patch
43daf92826a87b58fbafdaf282af4c2fddbf2a7d8c8c6491418ee962e4bccd6dcbbf8ff36b63f7b1ead181b957ba0f66e08e73c3e755397aafdbf99f3397da1e  pt-doc-fix.patch"
