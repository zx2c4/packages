# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmousetool
pkgver=19.08.2
pkgrel=0
pkgdesc="Tool to assist with clicking the mouse button"
url="https://userbase.kde.org/KMouseTool"
arch="all"
license="LGPL-2.1-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdbusaddons-dev ki18n-dev
	kdoctools-dev kiconthemes-dev knotifications-dev kxmlgui-dev phonon-dev
	libxtst-dev libxt-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kmousetool-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="a77de3d5a5e5601db7a276e4f14d11ac9d4a63a19231606fafcb959db34ce1d8232b3699dba6749fcf6f7f559f008b43e44e1cecf7746a7764695ec1b482f8ad  kmousetool-19.08.2.tar.xz"
