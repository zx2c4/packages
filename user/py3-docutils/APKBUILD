# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=py3-docutils
_pkgname=docutils
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=0.15.2
pkgrel=0
pkgdesc="Documentation utilities for Python"
url="https://pypi.python.org/pypi/docutils"
arch="noarch"
# Certified net clean
license="Custom"
depends="python3 py3-pillow"
makedepends="python3-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	cd "$builddir/test3"
	python3 alltests.py
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	# Remove .py extension from executable files.
	local path; for path in "$pkgdir"/usr/bin/*.py; do
		mv "$path" "${path%.py}"
	done
}

doc() {
	default_doc

	cd "$builddir"
	local docdir="$subpkgdir/usr/share/doc/$pkgname"
	mkdir -p "$docdir"
	cp -R docs/* "$docdir"
	cp *.txt "$docdir"

	local licdir="$subpkgdir/usr/share/licenses/$pkgname"
	mkdir -p "$licdir"
	rm -f licenses/docutils.conf
	cp licenses/* "$licdir"
}

sha512sums="b4528c7eba5a27e40f290a9df6894c277d11906d02f6842b9f364b29af9aa1e46f6008c87e4355947bcfa9f2db1cae9f38cf9fa7b8008ba45fa6d685922003a6  py3-docutils-0.15.2.tar.gz"
