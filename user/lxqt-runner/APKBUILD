# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lxqt-runner
pkgver=0.14.1
pkgrel=0
pkgdesc="Qt-based application launcher for LXQt"
url="https://lxqt.org"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.6.0
	liblxqt-dev>=${pkgver%.*}.0 lxqt-globalkeys-dev>=${pkgver%.*}.0
	muparser-dev kwindowsystem-dev menu-cache-dev qt5-qttools-dev
	qt5-qtsvg-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-runner/releases/download/$pkgver/lxqt-runner-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="62d196d37e87bc9154fa2cd9abcb8671277bffdb8673a6531bcb93161944cb8a31298587f8323d79a290292fc73765ea15990106828335879b87029ff1aba28c  lxqt-runner-0.14.1.tar.xz"
