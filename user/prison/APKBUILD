# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=prison
pkgver=5.65.0
pkgrel=0
pkgdesc="Qt Barcode library for programatically creating QR codes"
url="https://www.kde.org/"
arch="all"
license="MIT"
depends=""
depends_dev="qt5-qtbase-dev libqrencode-dev libdmtx-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qtdeclarative-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-quick"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/prison-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

quick() {
	pkgdesc="$pkgdesc (QML binding)"
	mkdir -p "$subpkgdir"/usr/lib/qt5/
	mv "$pkgdir"/usr/lib/qt5/qml "$subpkgdir"/usr/lib/qt5/
}

sha512sums="90840044073a2a2b6c3e6540ee0ca7b06ddcfbc13ed332590fca087cb0d78f660b0163ffd74d7b332ae99e07faa6417bcc3aa1b5a503246b501363f3738b7ce8  prison-5.65.0.tar.xz"
