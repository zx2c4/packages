# Contributor: k0r10n <k0r10n.dev@gmail.com>
# Maintainer: 
pkgname=ninja
pkgver=1.9.0
pkgrel=0
pkgdesc="Small build system with a focus on speed"
url="https://ninja-build.org/"
arch="all"
license="Apache-2.0"
depends=""
makedepends="asciidoctor gtest python3 re2c"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/martine/ninja/archive/v$pkgver.tar.gz
	posix-fix.patch::https://github.com/ninja-build/ninja/pull/1513/commits/567815df38a2ff54ad7478a90bd75c91e434236a.patch
	"

build() {
	python3 ./configure.py --bootstrap
	asciidoctor doc/manual.asciidoc
}

check() {
	./ninja ninja_test
	./ninja_test --gtest_filter=-SubprocessTest.SetWithLots
}

package() {
	install -m755 -D ninja "$pkgdir/usr/bin/ninja"
	install -m644 -D doc/manual.asciidoc \
		"$pkgdir/usr/share/doc/ninja/manual.asciidoc"
	install -m644 -D doc/manual.html \
		"$pkgdir/usr/share/doc/ninja/manual.html"

	install -m644 -D misc/bash-completion \
		"$pkgdir/usr/share/bash-completion/completions/ninja"
}

sha512sums="a8b5ad00b60bddbdb8439a6092c91a65d093f9bcd6241f69088eb35bea2847efe673c3107a130dc754c747c7712b839d8f88e88d8389520cf7143668ee053feb  ninja-1.9.0.tar.gz
78b29a57ec3c4feedf2bcfe9370b30cecd5302f29e8e14c6a3bc01becb555be6330aafba12f9225625af0644ea1f534feba8396d32bb944253a9daa5dd0e98d8  posix-fix.patch"
