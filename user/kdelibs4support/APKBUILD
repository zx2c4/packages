# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdelibs4support
pkgver=5.54.0
pkgrel=0
pkgdesc="Legacy support for KDE 4 software"
url="https://www.kde.org/"
arch="all"
options="!check suid"  # Test requires running X11 session.
license="LGPL-2.1+ AND MIT AND LGPL-2.1-only AND LGPL-2.0-only AND (LGPL-2.1-only OR LGPL-3.0-only) AND (LGPL-2.0-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtsvg-dev qt5-qttools-dev kcompletion-dev
	kconfigwidgets-dev kcrash-dev kdesignerplugin kdesignerplugin-dev
	kemoticons-dev kparts-dev kunitconversion-dev kinit kded kded-dev
	kitemmodels-dev knotifications-dev sonnet-dev threadweaver-dev
	kinit-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdelibs4support-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="860c2de36c1a1587c423eaa8f770be254d0458f3c223592ac07f116015ee2f597b913039180bf832e892cfc1060eb3830fe45c786466771441e3a1815a6a5065  kdelibs4support-5.54.0.tar.xz"
