# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=vlc
pkgver=3.0.8
pkgrel=0
pkgdesc="Multi-platform MPEG, VCD/DVD, and DivX player"
triggers="vlc-libs.trigger=/usr/lib/vlc/plugins"
pkgusers="vlc"
pkggroups="vlc"
url="https://www.videolan.org/vlc/"
arch="all"
license="GPL-2.0+"
options="textrel"
subpackages="$pkgname-dev $pkgname-doc $pkgname-qt $pkgname-pulse
	$pkgname-daemon::noarch $pkgname-libs $pkgname-lang"
depends="ttf-dejavu xdg-utils"
# Generic dependencies, then X11, then multimedia libraries
makedepends="autoconf automake bison eudev-dev flex libarchive-dev
	libgcrypt-dev libtool libxml2-dev lua5.3-dev ncurses-dev sysfsutils-dev

	dbus-dev freetype-dev fribidi-dev gtk+3.0-dev libice-dev libnotify-dev
	libjpeg-turbo-dev libsm-dev libx11-dev libxext-dev libxinerama-dev
	libxpm-dev libxv-dev mesa-dev qt5-qtbase-dev qt5-qtsvg-dev sdl2-dev
	qt5-qtx11extras-dev xcb-util-keysyms-dev xcb-util-renderutil-dev
	xdg-utils

	a52dec-dev alsa-lib-dev faad2-dev ffmpeg-dev flac-dev fluidsynth-dev
	gstreamer-dev gst-plugins-base-dev libaacs-dev libavc1394-dev
	libbluray-dev>=0.2.1 libbluray-dev<20100000 libcddb-dev libdc1394-dev
	libdca-dev libdvbpsi-dev libdvdnav-dev libdvdread-dev libmad-dev
	libmatroska-dev libmodplug-dev libmpeg2-dev libmtp-dev libogg-dev
	libraw1394-dev>=2.0.1 librsvg-dev libshout-dev libtheora-dev libva-dev
	libvdpau-dev libvorbis-dev live-media-dev opus-dev pulseaudio-dev
	speex-dev speexdsp-dev taglib-dev v4l-utils-dev x264-dev x265-dev"
replaces="vlc-plugins vlc-xorg"
source="https://get.videolan.org/vlc/$pkgver/vlc-$pkgver.tar.xz
	check-headless.patch
	disable-sub-autodetect-fuzzy-1-test.patch
	endian-fix.patch
	fix-testing.patch
	lua.patch
	omxil-rpi-codecs.patch
	tar-compat.patch
	test-s390x.patch

	$pkgname.initd
	$pkgname.confd
	"

# secfixes: vlc_media_player
#   3.0.4-r2:
#     - CVE-2018-19857
#   3.0.8-r0:
#     - CVE-2019-13602
#     - CVE-2019-13615
#     - CVE-2019-13962
#     - CVE-2019-14437
#     - CVE-2019-14438
#     - CVE-2019-14498
#     - CVE-2019-14533
#     - CVE-2019-14534
#     - CVE-2019-14535
#     - CVE-2019-14776
#     - CVE-2019-14777
#     - CVE-2019-14778
#     - CVE-2019-14970

prepare() {
	default_prepare
	NOCONFIGURE=1 ./bootstrap
}

build() {
	local _arch_opts=
	export CFLAGS="$CFLAGS -D_GNU_SOURCE"

	case "$CARCH" in
	arm*) _arch_opts="--enable-omxil --enable-omxil-vout --enable-rpi-omxil" ;;
	aarch64) _arch_opts="--enable-neon" ;;
	ppc64*) _arch_opts="--enable-altivec" ;;
	x86* | pmmx) _arch_opts="--disable-mmx --disable-sse" ;;
	esac

	BUILDCC="${CC:-gcc} -std=c99" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-nls \
		--enable-optimizations \
		--enable-optimize-memory \
		--disable-rpath \
		--disable-vpx \
		--disable-wayland \
		--enable-dbus \
		--enable-notify \
		--enable-ncurses \
		--enable-pulse \
		--enable-qt \
		--enable-udev \
		--enable-a52 \
		--enable-avcodec \
		--enable-avformat \
		--enable-bluray \
		--enable-dc1394 \
		--enable-dca \
		--enable-dvbpsi \
		--enable-dvdnav \
		--enable-dvdread \
		--enable-faad \
		--enable-flac \
		--enable-fluidsynth \
		--enable-jpeg \
		--enable-libcddb \
		--enable-libmpeg2 \
		--enable-libva \
		--enable-live555 \
		--enable-mad \
		--enable-merge-ffmpeg \
		--enable-ogg \
		--enable-opus \
		--enable-png \
		--enable-realrtsp \
		--enable-shout \
		--enable-skins2 \
		--enable-speex \
		--enable-sout \
		--enable-taglib \
		--enable-theora \
		--enable-v4l2 \
		--enable-vdpau \
		--enable-vlm \
		--enable-vorbis \
		--enable-wma-fixed \
		--enable-x264 \
		--enable-x265 \
		--enable-xvideo \
		$_arch_opts

	make
}

package() {
	make DESTDIR="$pkgdir" install
	# delete cache as it's autocreated by trigger
	rm -rf "$pkgdir"/usr/lib/vlc/plugins/plugins.dat
	# delete unneeded mozilla and kde support files
	rm -rf "$pkgdir"/usr/lib/mozilla
	rm -rf "$pkgdir"/usr/share/kde4
}

check() {
	make check
}

_mv() {
	local dir="${1%/*}"
	mkdir -p "$subpkgdir"/$dir
	mv "$1" "$subpkgdir"/$dir/
}

pulse() {
	pkgdesc="PulseAudio support for VLC"
	depends=""
	install_if="vlc=$pkgver-r$pkgrel pulseaudio"
	mkdir -p "$subpkgdir"/usr/lib/vlc
	mv "$pkgdir"/usr/lib/vlc/libvlc_pulse* "$subpkgdir"/usr/lib/vlc/
}

qt() {
	pkgdesc="Qt frontend for VLC"
	depends="vlc=$pkgver-r$pkgrel"
	cd "$pkgdir"
	# scan for elf files that directly or indirectly depends on
	# libQt* libraries
	cd "$pkgdir"
	for i in $(find . -type f ); do
		if ldd $i 2>/dev/null | grep -q "libQt"; then
			_mv "$i" || return 1
		fi
	done
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/qvlc \
		"$subpkgdir"/usr/bin/
}

daemon() {
	pkgdesc="Support for running VLC as a daemon"
	install="vlc-daemon.pre-install"
	depends="vlc=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"
	cd "$pkgdir"
	install -D -m755 "$srcdir"/vlc.initd "$subpkgdir"/etc/init.d/vlc
	install -D -m664 "$srcdir"/vlc.confd "$subpkgdir"/etc/conf.d/vlc
	install -d -o vlc -g vlc "$subpkgdir"/var/log/vlc
}

libs() {
	depends=
	mkdir -p "$subpkgdir"/usr/lib/vlc
	mv "$pkgdir"/usr/lib/vlc/vlc-cache-gen \
		"$subpkgdir"/usr/lib/vlc/
	default_libs
}

sha512sums="5ade0b350e98fd6fa90035bffabda96f0addb3844a7c0a242b4db1cab6a746e1adb1d713ddcb48ae51a7d1736090f096f5d3b0637a9f958ccf4fcf27e838cf70  vlc-3.0.8.tar.xz
22d80df599b8b65a5439cefbb7140af8e9530f326d54945da3769af65f37518b99ec2cc8647aafd2763324a0698280915afe043cc87e5720c4694881ed35bffa  check-headless.patch
e214b407235cb3afb8bec93f20c9b42957b57e6fd3960679d3d4235e77762e03e64d03c01f00ef63d589e7c85aaad02ce6abbeeccd66b1867bc92451a5b5e9b0  disable-sub-autodetect-fuzzy-1-test.patch
e063c727d952465bbea33f669db49190427521dc8e2291e9a5cbb0f5e8e879bd3ba76855e44bd4630948e30c4329d27bd928f95de20fe1050d5e839778a4d012  endian-fix.patch
63adb16b3a1927ee3de27ac339cbfbbaa346a69928c527f883259d1e03b5cb59f26a55feeda767837b448c455de584abcb53dc733b2845c0cc13621d72e7f6fd  fix-testing.patch
35f83e38a6a0dd1e3c37e3dc6d63b1406d2f9454ed246854c1408d6f35ad74b743c5b0dbc19442bab65aad4268707ffa85bfda9e72b2d711c1d3412d955bf150  lua.patch
e13e398b7bfd977f6e099bcb6cf8dc5cd5bad6dea3eff715881826246dc4329468846084aff2576de2b7fd28d3f06e7c327a6e4511a28d22e5cd198a81146c89  omxil-rpi-codecs.patch
a117ca4d7fd66a5f959fdeaddfdce2f8442fe9f2c13995bb7f4792a7745c00813813aa962f76e957e3b0735344a5dc000e0644ce09f23458802a2932231655c3  tar-compat.patch
c0107655249687655846a9547ca1a5670b9207443180600e7a149c69ffb96d7226787c19b018d4033db9b284c1a5faa8d7d42188ed40c3b8bb051256febf11c5  test-s390x.patch
55e245190b443dde9c7215ea5210612fcca164900a9a4b025ccf0d1e3fc5206d00b52355b256974421e37c609875627f1db19f0f5a084511aec0daf677ecc9d6  vlc.initd
d89190dca1b8b2c3faca5863dc6c7e6eb24e05178e6f75ed752fd3c6a73cb8a42d2625b6e56453296b7096ea868be642ecd42745dac20e7f13fc67dd3c3c7c49  vlc.confd"
