# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kbounce
pkgver=19.08.2
pkgrel=0
pkgdesc="Puzzle/arcade game to build walls"
url="https://games.kde.org/game.php?game=kbounce"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcoreaddons-dev kcrash-dev kwidgetsaddons-dev kdbusaddons-dev ki18n-dev
	kconfigwidgets-dev kcompletion-dev kxmlgui-dev kio-dev kdoctools-dev
	libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kbounce-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="745836150ecca4937cac66ff69da813c1b06bc92ae8a20198857362abeb156ceb809fd2a83429c9f8f4bbdba23261751ebdf921a768d3c0be27493e0a423167f  kbounce-19.08.2.tar.xz"
