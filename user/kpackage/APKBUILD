# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpackage
pkgver=5.65.0
pkgrel=0
pkgdesc="Frameworks for managing KDE data packages"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires Plasma to be installed, causing circular dep
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev karchive-dev ki18n-dev kcoreaddons-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 kdoctools-dev
	qt5-qttools-dev doxygen graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpackage-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="789fa54ddc4a117fb1d233f7810fdf5c85a6aa5e41c46766ae8d93f60322c441611927a5e163cc8fa6f36780ad06cdbb82d82db80ab6eca4720a45536164101f  kpackage-5.65.0.tar.xz"
