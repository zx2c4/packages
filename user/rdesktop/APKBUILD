# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=rdesktop
pkgver=1.8.6
pkgrel=0
pkgdesc="Remote Desktop Protocol client"
url="https://www.rdesktop.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-3.0-only"
depends=""
makedepends="alsa-lib-dev krb5-dev libice-dev libsamplerate-dev libx11-dev
	libxrandr-dev openssl-dev"
subpackages="$pkgname-doc"
source="https://github.com/rdesktop/rdesktop/releases/download/v$pkgver/rdesktop-$pkgver.tar.gz
	gssapi.patch
	signed-int.patch
	"

prepare() {
	default_prepare
	update_config_sub
	./bootstrap
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-smartcard
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="18b918883746c490852b97086e7727d546a0b1902a87515d6ff7b9568f30404a24934ea3ac55c9213d4db7bf5be3b47985490fa0533b15d9d6749d31c77260a6  rdesktop-1.8.6.tar.gz
d55709968b21b64fe33bc1fe90156515e88fe715cbaad6f9c7a15ce4b26e09397821708139748c744ba8bb12f1e751602189317c583ff2017941f39360b4b8cd  gssapi.patch
e8b4af70a54944d83b7c899aa680042f559e75af3e9a3deb2c7395f8b4a56e50d1c2f26bd10b2377ff577115d635c2aa0fdbddf995588f1d492badfc3e72456e  signed-int.patch"
