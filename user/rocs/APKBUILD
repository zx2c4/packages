# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=rocs
pkgver=19.08.2
pkgrel=0
pkgdesc="Graph theory IDE"
url="https://www.kde.org/applications/education/rocs/"
arch="all"
options="!check"  # All tests require X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdoctools-dev boost-dev
	grantlee-dev qt5-qtwebkit-dev qt5-qtsvg-dev qt5-qtxmlpatterns-dev
	karchive-dev kconfig-dev kcoreaddons-dev kcrash-dev kdeclarative-dev
	ki18n-dev kitemviews-dev ktexteditor-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/rocs-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="2832a0dfe83076100357c86eeb3ad0fccf55f667bd4b86c0cd8bd2db21f8b323a524d3016db9509f7bfee1837820d9bcdac0dc4dd8a4e5d2af020f8102b4aa0d  rocs-19.08.2.tar.xz"
