# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: 
pkgname=snappy
pkgver=1.1.7
pkgrel=0
pkgdesc="Fast compression and decompression library"
url="https://google.github.io/snappy/"
arch="all"
[ "$CARCH" = "armhf" ] && options="!check"     # does not pass testsuite on armhf
license="BSD-3-Clause"
subpackages="$pkgname-dbg $pkgname-dev"
source="snappy-$pkgver.tar.gz::https://github.com/google/snappy/archive/$pkgver.tar.gz
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="32046f532606ba545a4e4825c0c66a19be449f2ca2ff760a6fa170a3603731479a7deadb683546e5f8b5033414c50f4a9a29f6d23b7a41f047e566e69eca7caf  snappy-1.1.7.tar.gz"
