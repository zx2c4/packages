# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=s6-networking
pkgver=2.3.1.1
pkgrel=0
pkgdesc="skarnet.org's UCSPI TCP tools, access control tools, and network time management utilities."
url="https://skarnet.org/software/$pkgname/"
arch="all"
options="!check"  # No test suite.
license="ISC"
_skalibs_version=2.9
depends="execline"
makedepends="skalibs-dev>=$_skalibs_version skalibs-libs-dev>=$_skalibs_version execline-dev s6-dev s6-libs-dev s6-dns-dev s6-dns-libs-dev bearssl-dev"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
        cd "$builddir"
        ./configure \
                --enable-shared \
                --enable-static \
                --disable-allstatic \
                --prefix=/usr \
		--libdir=/usr/lib \
                --libexecdir="/usr/lib/$pkgname" \
                --with-dynlib=/lib \
                --enable-ssl=bearssl
        make
}

package() {
        cd "$builddir"
        make DESTDIR="$pkgdir" install
}


libs() {
        pkgdesc="$pkgdesc (shared libraries)"
        depends="skalibs-libs>=$_skalibs_version"
        mkdir -p "$subpkgdir/usr/lib"
        mv "$pkgdir"/usr/lib/*.so.* "$subpkgdir/usr/lib/"
}


dev() {
        pkgdesc="$pkgdesc (development files)"
        depends="skalibs-dev>=$_skalibs_version"
        mkdir -p "$subpkgdir/usr/include" "$subpkgdir/usr/lib"
        mv "$pkgdir/usr/include" "$subpkgdir/usr/"
        mv "$pkgdir"/usr/lib/*.a "$subpkgdir/usr/lib/"
}


libsdev() {
        pkgdesc="$pkgdesc (development files for dynamic linking)"
        depends="$pkgname-dev"
        mkdir -p "$subpkgdir/usr/lib"
        mv "$pkgdir"/usr/lib/*.so "$subpkgdir/usr/lib/"
}


doc() {
        pkgdesc="$pkgdesc (documentation)"
        depends=
        install_if="docs $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/share/doc"
        cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="3bd7cdbf6f105bb83c5116e736f4288d378571467e7aa93f862eba80ce28255bda77140a2a1cf80af80480789dfb38682049f6769952fb8964b860bdf465551b  s6-networking-2.3.1.1.tar.gz"
