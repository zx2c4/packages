# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=ffmpegthumbnailer
pkgver=2.2.2
pkgrel=0
pkgdesc="Thumbnail generator based on FFMPEG"
url="https://github.com/dirkvdb/ffmpegthumbnailer"
arch="all"
license="GPL-2.0+"
makedepends="ffmpeg-dev libjpeg-turbo-dev libpng-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/dirkvdb/ffmpegthumbnailer/releases/download/$pkgver/ffmpegthumbnailer-$pkgver.tar.bz2"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_GIO=True \
		-DENABLE_THUMBNAILER=True \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="52760dcf59430e5e85024c9f19dc1fac1d5f0edb4f937b33feac2b3ca8f12bbf549b5f658fc16fc07bf773717b9e10048aa3eb24bf52811c5c88c995ef492612  ffmpegthumbnailer-2.2.2.tar.bz2"
