# Contributor: Kiyoshi Aman <kiyoshi.aman@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=protobuf
_gemname=google-protobuf
pkgver=3.10.0
_tstver=1.8.1
pkgrel=0
pkgdesc="Library for extensible, efficient structure packing"
url="https://github.com/google/protobuf"
arch="all"
options="!check"  # Broken everywhere.
license="BSD-3-Clause"
depends_dev="zlib-dev"
makedepends="$depends_dev autoconf automake libtool ruby ruby-dev ruby-rake
	ruby-rake-compiler python3 python3-dev"
checkdepends="ruby-json ruby-test-unit"
subpackages="ruby-$_gemname:_ruby py3-$pkgname:_python $pkgname-dev $pkgname-vim::noarch"
source="$pkgname-$pkgver.tar.gz::https://github.com/google/$pkgname/archive/v$pkgver.tar.gz
	googletest-$_tstver.tar.gz::https://github.com/google/googletest/archive/release-$_tstver.tar.gz"

prepare() {
	default_prepare

	cd "$builddir"
	./autogen.sh

	# symlink tests to the test directory
	rm -rf third_party/*
	ln -sf "$srcdir"/googletest-release-$_tstver \
		"$builddir"/third_party/googletest
}

build() {
	# Build Protobuf
	cd "$builddir"
	CXXFLAGS="$CXXFLAGS -fno-delete-null-pointer-checks" LDFLAGS="$LDFLAGS -latomic" \
		./configure --prefix=/usr \
			--sysconfdir=/etc \
			--mandir=/usr/share/man \
			--infodir=/usr/share/info \
			--localstatedir=/var
	make

	# Build for Ruby
	cd "$builddir"/ruby
	# Generate proto files for built-in protocols.
	rake genproto
	gem build $_gemname.gemspec
	gem install --local \
		--install-dir dist \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	# Build for Python 3
	cd "$builddir"/python
	export LD_LIBRARY_PATH=${builddir}/src/.libs
	python3 setup.py build --cpp_implementation

	# Build test-suite
	local test; for test in googletest googlemock; do
		cd "$builddir/third_party/googletest/$test"
		autoreconf -vfi
		./configure
		make
	done
}

check() {
	cd "$builddir"
	make check
	cd "$builddir"/ruby
	rake test
	cd "$builddir"/python
	python3 setup.py test --cpp_implementation
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

_ruby() {
	pkgdesc="Ruby bindings to Google's data interchange format"

	local gemdir="$subpkgdir/$(ruby -e 'puts Gem.default_dir')"
	cd "$builddir"/ruby/dist

	mkdir -p "$gemdir"
	cp -r extensions gems specifications "$gemdir"/

	# Remove duplicated .so libs (should be only in extensions directory).
	find "$gemdir"/gems/ -name "*.so" -delete

	# Remove unnecessary files.
	cd "$gemdir"/gems/$_gemname-$pkgver
	rm -r ext/ tests/
}

_python() {
	pkgdesc="Python bindings to Google's data interchange format"

	cd "$builddir"/python
	python3 setup.py install --prefix=/usr --root="$subpkgdir" \
		--cpp_implementation
}

vim() {
	pkgdesc="Vim syntax for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel vim"

	install -Dm644 "$builddir"/editors/proto.vim \
		"$subpkgdir"/usr/share/vim/vimfiles/syntax/proto.vim
}

sha512sums="0dcba6d21486fdc162f57119754b47b4a2fb605af878d5b96a32df55895321535cffb5b804566fd90ee7c36e20106d0cd4f5d9f3c652dc9c4dfca96be41a1977  protobuf-3.10.0.tar.gz
e6283c667558e1fd6e49fa96e52af0e415a3c8037afe1d28b7ff1ec4c2ef8f49beb70a9327b7fc77eb4052a58c4ccad8b5260ec90e4bceeac7a46ff59c4369d7  googletest-1.8.1.tar.gz"
