# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=liblxqt
pkgver=0.14.1
pkgrel=0
pkgdesc="Core LXQt library"
url="https://lxqt.org"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	qt5-qttools-dev libxscrnsaver-dev libqtxdg-dev>=3.3.1
	kwindowsystem-dev polkit-qt-1-dev lxqt-build-tools>=0.6.0"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/liblxqt/releases/download/$pkgver/liblxqt-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="28ce5e924d136f9907f84e40d4823c412df071009a9d5da4e061ce969d7575b21210d5178103f9bfc73baa87ccafc87dcb6e29a737467ca1a459c1fd3c1071ba  liblxqt-0.14.1.tar.xz"
