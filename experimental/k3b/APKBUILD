# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=k3b
pkgver=17.12.2
pkgrel=0
pkgdesc="CD and DVD burner and copier"
url="https://userbase.kde.org/K3b"
arch="all"
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kconfig-dev
	kcoreaddons-dev kdoctools-dev kfilemetadata-dev ki18n-dev kio-dev
	kiconthemes-dev kjobwidgets-dev kcmutils-dev knotifications-dev
	knewstuff-dev knotifyconfig-dev kservice-dev solid-dev kxmlgui-dev
	kwidgetsaddons-dev qt5-qtwebkit-dev taglib-dev flac-dev ffmpeg-dev
	libdvdread-dev libsndfile-dev lame-dev libogg-dev libsamplerate-dev
	libmad-dev libkcddb-dev"
install=""
subpackages="$pkgname-doc $pkgname-lang"
source="http://download.kde.org/stable/applications/$pkgver/src/k3b-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="2e1de62fab8e1e702b9a7c3431e44c2809376a329ba9236e056336d26c885b37e956059375d5f26f1b5c94093ccca80524d497a4b0328f277204293461b249de  k3b-17.12.2.tar.xz"
