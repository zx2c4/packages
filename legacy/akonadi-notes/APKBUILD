# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi-notes
pkgver=18.04.3
pkgrel=0
pkgdesc="Library for integrating notes into Akonadi"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1"
depends=""
depends_dev="qt5-qtbase-dev ki18n-dev kmime-dev akonadi-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/akonadi-notes-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="0aecf012ae8bc03ffc2ba5ba68893486d4ea91cc2d6efecccc3fc60b7f03a51596475285e0fa2e1b9b025b3b046cdca4f4d248e8aa00fb08c0176269ca067177  akonadi-notes-18.04.3.tar.xz"
