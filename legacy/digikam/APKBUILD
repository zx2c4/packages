# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=digikam
pkgver=5.9.0
pkgrel=0
pkgdesc="Professional photo management and digital camera import"
url="https://www.digikam.org/"
arch="all"
options="!check"  # Test suite doesn't support our version of Boost
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtxmlpatterns-dev
	qt5-qtsvg-dev kconfig-dev kwindowsystem-dev kxmlgui-dev ki18n-dev
	karchive-dev kio-dev qt5-qtwebkit-dev kcoreaddons-dev kservice-dev
	solid-dev kiconthemes-dev kfilemetadata-dev threadweaver-dev libpng-dev
	knotifyconfig-dev knotifications-dev akonadi-contacts-dev kcalcore-dev
	libjpeg-turbo-dev tiff-dev zlib-dev boost-dev lcms2-dev expat-dev
	exiv2-dev flex bison libxml2-dev libxslt-dev eigen-dev libgphoto2-dev
	libksane-dev libkipi-dev glu-dev qt5-qtx11extras-dev jasper-dev
	opencv opencv-dev"
# YES, both are needed. opencv-dev only pulls in -libs; CMake module in opencv
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/digikam/digikam-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_APPSTYLES=ON \
		-DENABLE_MYSQLSUPPORT=OFF \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		-DENABLE_OPENCV3=ON \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS}
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="f6134a957dcd11343e34f0bc90f5560a28ed205c47d2ffd76a5418a4c63f76706045e9179c47f1448304b59081051b524f7a6847c3c1dcee4673047ce5d90e63  digikam-5.9.0.tar.xz"
