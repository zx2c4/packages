# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=console-setup
pkgver=1.194
pkgrel=0
pkgdesc="Tools for configuring the console using X Window System key maps"
url="https://salsa.debian.org/installer-team/console-setup"
arch="noarch"
options="!check"  # No test suite.
license="MIT AND GPL-2.0+ AND BSD-3-Clause"
depends="ckbcomp kbd"
makedepends="perl"
subpackages="$pkgname-doc console-fonts:fonts console-keymaps:keymaps
	$pkgname-openrc ckbcomp"
source="http://ftp.de.debian.org/debian/pool/main/c/$pkgname/${pkgname}_$pkgver.tar.xz
	console-setup.initd
	"

build() {
	make build-linux
}

package() {
	make prefix="$pkgdir/usr" etcdir="$pkgdir/etc" xkbdir= install-linux
	install -D -m755 "$srcdir"/console-setup.initd "$pkgdir"/etc/init.d/console-setup
}

fonts() {
	pkgdesc="Console fonts for use with $pkgname"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/consolefonts "$subpkgdir"/usr/share/
}

keymaps() {
	pkgdesc="Keyboard layouts for use with $pkgname"
	mkdir -p "$subpkgdir"/etc/console-setup
	mv "$pkgdir"/etc/console-setup/ckb "$subpkgdir"/etc/console-setup/
}

ckbcomp() {
	pkgdesc="XKB keyboard layout translation utility"
	depends="perl"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/ckbcomp "$subpkgdir"/usr/bin/
}

sha512sums="fe71480b45e8c88c9e46264f734d0eb21023a6afedfa6412551bb40f7525f14823befe726efc37f152a36d9a8de3aba00cd41f9fe42bcea2780c5cf8764a6b80  console-setup_1.194.tar.xz
3b8e2c9d8551f9a51bcd33e58771a4f55ff2840f8fe392e0070bd2b6a3911cd9ed9377873538f6904fd99836ac4e0280c712be69d275aae9183dd12ff7efddae  console-setup.initd"
