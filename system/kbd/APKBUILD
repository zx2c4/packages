# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kbd
pkgver=2.2.0_git20190823
pkgrel=0
pkgdesc="Console keyboard and font management utilities"
url=" "
arch="all"
options="!check"  # padding error on at least ppc64
license="GPL-2.0+"
depends=""
checkdepends="check-dev"
makedepends="linux-headers linux-pam-dev autoconf automake libtool"
subpackages="$pkgname-doc $pkgname-fonts::noarch $pkgname-keymaps::noarch
	$pkgname-lang"
source="https://dev.sick.bike/dist/$pkgname-$pkgver.tar.xz"

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

fonts() {
	pkgdesc="Fonts shipped with Linux kbd tools"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/consolefonts "$subpkgdir"/usr/share/
}

keymaps() {
	pkgdesc="Legacy keymaps shipped with Linux kbd tools"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/keymaps "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/unimaps "$subpkgdir"/usr/share/
}

sha512sums="7f6202aeb17d6224095447a92d999478220457e1e7cadb90a7c40ca7f3b0c5b1f672db1995fb69652ca37558a75df582bfb5fea5772f3b1e040fe39f8f54504e  kbd-2.2.0_git20190823.tar.xz"
