# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=man-db
pkgver=2.8.6.1
pkgrel=0
pkgdesc="The man command and related utilities for examining on-line help files"
url="https://www.nongnu.org/man-db/"
arch="all"
options="!check"  # requires //IGNORE in iconv
license="GPL-2.0+"
depends="groff gzip less"
makedepends_host="db-dev libpipeline-dev zlib-dev"
subpackages="$pkgname-doc $pkgname-lang"
triggers="man-db.trigger=/usr/share/man"
source="https://download.savannah.nongnu.org/releases/man-db/man-db-$pkgver.tar.xz
	man-db-2.8.5-iconv.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-setuid \
		--with-sections="1 1p 1x 2 2x 3 3p 3x 4 4x 5 5x 6 6x 7 7x 8 8x 9 0p tcl n l p o" \
		--enable-nls \
		--with-db=db
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	rm -r "${pkgdir}"/usr/lib/tmpfiles.d  # systemd
}

sha512sums="0c43cdddf2e07cd4a2225e098ea4cbfb2842a9bbf08bfb073058be0d66efac3a77ca0f8725564677c8f61e671f471c76f1dbba77a7601d7a63bb81350ef51bb5  man-db-2.8.6.1.tar.xz
76a5b13d3018627cbc5a72cb51da3c78022245ad574c742699e7c4ab152ec2222ce7f34af5594de74b436dd23008af6c43739ee66973e049b07d82882e4965c6  man-db-2.8.5-iconv.patch"
