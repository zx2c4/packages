# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tzdata
pkgver=2019c
_tzcodever=2019c
_ptzver=0.5
pkgrel=0
pkgdesc="Time zone data"
url="https://www.iana.org/time-zones"
arch="all"
options="!check"  # Until we have sgml-common.
license="Public-Domain"
depends=""
checkdepends="sgml-common sp"
makedepends=""
subpackages="$pkgname-doc"
source="https://www.iana.org/time-zones/repository/releases/tzcode$_tzcodever.tar.gz
	https://www.iana.org/time-zones/repository/releases/tzdata$pkgver.tar.gz
	https://dev.alpinelinux.org/archive/posixtz/posixtz-$_ptzver.tar.xz
	0001-posixtz-fix-up-lseek.patch"

builddir="$srcdir"
_timezones="africa antarctica asia australasia europe northamerica \
	southamerica pacificnew etcetera backward systemv factory"

build() {
	make cc="${CC:-gcc}" CFLAGS="$CFLAGS -DHAVE_STDINT_H=1"
		TZDIR="/usr/share/zoneinfo"

	make -C posixtz-$_ptzver posixtz
}

check() {
	make -j1 check
}

package() {
	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo ${_timezones}
	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo/right -L leapseconds ${_timezones}
	#./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo/posix ${_timezones}

	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo -p America/New_York
	install -m444 -t "$pkgdir"/usr/share/zoneinfo iso3166.tab zone1970.tab zone.tab

	mkdir -p "$pkgdir"/usr/sbin
	install -m755 zic zdump "$pkgdir"/usr/sbin

	mkdir -p "$pkgdir"/usr/share/man/man8
	install -m644 zic.8 zdump.8 "$pkgdir"/usr/share/man/man8

	rm -f "$pkgdir"/usr/share/zoneinfo/localtime
	install -Dm755 "$srcdir"/posixtz-$_ptzver/posixtz \
		"$pkgdir"/usr/bin/posixtz
}

sha512sums="61ef36385f501c338c263081486de0d1fccd454b86f8777b0dbad4ea3f21bbde059d0a91c23e207b167ed013127d3db8b7528f0188814a8b44d1f946b19d9b8b  tzcode2019c.tar.gz
2921cbb2fd44a6b8f7f2ed42c13fbae28195aa5c2eeefa70396bc97cdbaad679c6cc3c143da82cca5b0279065c02389e9af536904288c12886bf345baa8c6565  tzdata2019c.tar.gz
68dbaab9f4aef166ac2f2d40b49366527b840bebe17a47599fe38345835e4adb8a767910745ece9c384b57af815a871243c3e261a29f41d71f8054df3061b3fd  posixtz-0.5.tar.xz
f54ce213d74c5a8387e1a7c56299bc6eee65a035772288222128abc249a112067b8791b88b45c342b2d4d8d12e9e4f1f2f5c92c5de67f8b6413b1ebf1d7de467  0001-posixtz-fix-up-lseek.patch"
