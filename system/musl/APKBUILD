# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=musl
pkgver=1.2.0_alpha3
pkgrel=0
pkgdesc="System library (libc) implementation"
url="https://www.musl-libc.org/"
arch="all"
options="!check"
license="MIT"
depends=""
makedepends=""
subpackages="$pkgname-dev"
case "$BOOTSTRAP" in
nocc)	pkgname="musl-dev"
	subpackages=""
	options="$options !dbg"
	builddir="$srcdir"/musl-$pkgver
	;;
nolibc) ;;
*)	subpackages="$subpackages $pkgname-utils"
	triggers="$pkgname-utils.trigger=/etc/ld.so.conf.d"
	;;
esac
source="https://distfiles.adelielinux.org/source/$pkgname-$pkgver.tar.xz
	amalgamation.patch
	3001-make-real-lastlog-h.patch
	handle-aux-at_base.patch
	fgetspent_r.patch

	ldconfig
	getent.c
	iconv.c
	"

# secfixes:
#   1.1.15-r4:
#     - CVE-2016-8859
#   1.1.23-r2:
#     - CVE-2019-14697

build() {
	[ "$BOOTSTRAP" = "nocc" ] && return 0

	if [ "$BOOTSTRAP" != "nolibc" ]; then
		# getconf/getent/iconv
		local i
		for i in getent iconv ; do
			${CROSS_COMPILE}gcc $CPPFLAGS $CFLAGS "$srcdir"/$i.c -o $i
		done
	fi

	# note: not autotools
	LDFLAGS="$LDFLAGS -Wl,-soname,libc.musl-${CARCH}.so.1" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
}

package() {
	if [ "$BOOTSTRAP" = "nocc" ]; then
		case "$CARCH" in
		aarch64*)	ARCH="aarch64" ;;
		arm*)		ARCH="arm" ;;
		x86)		ARCH="i386" ;;
		x86_64)		ARCH="x86_64" ;;
		ppc)		ARCH="powerpc" ;;
		ppc64*)		ARCH="powerpc64" ;;
		s390*)		ARCH="s390x" ;;
		mips64*)	ARCH="mips64" ;;
		mips*)		ARCH="mips" ;;
		m68k)		ARCH="m68k" ;;
		esac

		make ARCH="$ARCH" prefix=/usr DESTDIR="$pkgdir" install-headers
	else
		make DESTDIR="$pkgdir" install

		# make LDSO the be the real file, and libc the symlink
		local LDSO="$(make -f Makefile --eval "$(printf 'print-ldso:\n\t@echo $$(basename $(LDSO_PATHNAME))')" print-ldso)"
		mv -f "$pkgdir"/usr/lib/libc.so "$pkgdir"/lib/"$LDSO"
		ln -sf "$LDSO" "$pkgdir"/lib/libc.musl-${CARCH}.so.1
		ln -sf ../../lib/"$LDSO" "$pkgdir"/usr/lib/libc.so
		mkdir -p "$pkgdir"/usr/bin
		ln -sf ../../lib/"$LDSO" "$pkgdir"/usr/bin/ldd
	fi

	rm "$pkgdir"/usr/include/utmp.h     # utmps
	rm "$pkgdir"/usr/include/utmpx.h    # utmps
}

dev() {
	provides="libc-dev=$pkgver-r$pkgrel"
	default_dev
}

utils() {
	depends="!uclibc-utils scanelf"
	provides="libc-utils=$pkgver-r$pkgrel"
	replaces="libiconv uclibc-utils"
	license="BSD-2-Clause AND GPL-2.0+"

	mkdir -p "$subpkgdir"/usr "$subpkgdir"/sbin
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/

	install -D \
		"$builddir"/getent \
		"$builddir"/iconv \
		"$subpkgdir"/usr/bin

	install -D -m755 "$srcdir"/ldconfig "$subpkgdir"/sbin
}

sha512sums="86b42aa86a08699cc5139fca7d2420da5ad3fc31f18454bd6167ad09b9ca69e2122657b7572f6bc1b68393df9756b9b6d2f8c1685e6e4aa170a8c04da32d1cbc  musl-1.2.0_alpha3.tar.xz
f01ab92b9d385c15369c0bb7d95e1bc06a009c8851e363517d0ba1bae3fc2647af69fc2f363b5d962d376c5d9a1994b5728fd88ccbfff5f0d3d0970a02df1512  amalgamation.patch
88ae443dbb8e0a4368235bdc3a1c5c7b718495afa75e06deb8e01becc76cb1f0d6964589e2204fc749c9c1b3190b8b9ac1ae2c0099cab8e2ce3ec877103d4332  3001-make-real-lastlog-h.patch
6a7ff16d95b5d1be77e0a0fbb245491817db192176496a57b22ab037637d97a185ea0b0d19da687da66c2a2f5578e4343d230f399d49fe377d8f008410974238  handle-aux-at_base.patch
ded41235148930f8cf781538f7d63ecb0c65ea4e8ce792565f3649ee2523592a76b2a166785f0b145fc79f5852fd1fb1729a7a09110b3b8f85cba3912e790807  fgetspent_r.patch
cb71d29a87f334c75ecbc911becde7be825ab30d8f39fa6d64cb53812a7c9abaf91d9804c72540e5be3ddd3c84cfe7fd9632274309005cb8bcdf9a9b09b4b923  ldconfig
378d70e65bcc65bb4e1415354cecfa54b0c1146dfb24474b69e418cdbf7ad730472cd09f6f103e1c99ba6c324c9560bccdf287f5889bbc3ef0bdf0e08da47413  getent.c
9d42d66fb1facce2b85dad919be5be819ee290bd26ca2db00982b2f8e055a0196290a008711cbe2b18ec9eee8d2270e3b3a4692c5a1b807013baa5c2b70a2bbf  iconv.c"
