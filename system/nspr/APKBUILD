# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=nspr
pkgver=4.22
pkgrel=0
pkgdesc="Netscape Portable Runtime"
url="https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSPR"
arch="all"
options="!check"  # No test suite.
license="MPL-1.1 AND GPL-2.0-only AND LGPL-2.1-only"
depends=""
# -dev package does not ship any symlinks so dependency cannot be autodetected
depends_dev="nspr"
makedepends="autoconf automake"
subpackages="$pkgname-dev"
source="https://ftp.mozilla.org/pub/mozilla.org/nspr/releases/v$pkgver/src/nspr-$pkgver.tar.gz
	fix-getproto.patch
	stacksize.patch
	"

prepare() {
	mkdir build inst
	default_prepare
}

build() {
	case "$CARCH" in
		*64* | s390x) _conf="--enable-64bit";;
	esac
	cd "$builddir"/build
	# ./nspr/pr/include/md/_linux.h tests only __GLIBC__ version
	# to detect c-library features, list musl features here for now.
	CFLAGS="$CFLAGS -D_PR_POLL_AVAILABLE -D_PR_HAVE_OFF64_T -D_PR_INET6 -D_PR_HAVE_INET_NTOP -D_PR_HAVE_GETHOSTBYNAME2 -D_PR_HAVE_GETADDRINFO -D_PR_INET6_PROBE" \
	../nspr/configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-debug \
		--enable-optimize \
		--enable-ipv6 \
		$_conf
	make CC="${CC:-gcc}" CXX="${CXX:-g++}"
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install

	cd "$pkgdir"/usr/lib
	rm -f *.a

	cd "$builddir"/build/config
	install -Dm755 nspr-config "$pkgdir"/usr/bin/nspr-config
	install -Dm644 nspr.pc "$pkgdir"/usr/lib/pkgconfig/nspr.pc
	rm -rf "$pkgdir"/usr/bin/prerr.properties \
		"$pkgdir"/usr/bin/compile-et.pl \
		"$pkgdir"/usr/share/aclocal/nspr.m4 \
		"$pkgdir"/usr/include/nspr/md
}

sha512sums="d34271f0692905162f71b02cf6f62a8ebf779159335e4ed286ee8d51faaad633a61c9392bada86d976bca2b5aa47c8760085de30dbd13c9cfa423582c31db37b  nspr-4.22.tar.gz
ee654f609a90b9a95c1901ad1d56cdef7cce8bca9efc092198607944f142f8fec924b13219ca6663facd0f0d59b671b8b3de58ab3449c4c863d42e2937f02184  fix-getproto.patch
1f694fc151f6578080449e3aa999c520486bbe117b8237150966ec43092db4156e81412ac889045e0c0c3bf65d459af5bdc1cf19c9fa3dab120405a60732f15a  stacksize.patch"
